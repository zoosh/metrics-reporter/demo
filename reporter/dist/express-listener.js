"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const lib_1 = require("zoosh-metrics-reporter/lib");
const port = 3001;
const app = (0, express_1.default)();
app.get('/metrics', (req, res, next) => {
    lib_1.MetricsReporterExpress.listener(req, res, []);
});
app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});
//# sourceMappingURL=express-listener.js.map