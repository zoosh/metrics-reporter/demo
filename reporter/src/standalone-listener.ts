import { MetricsReporterStandalone } from 'zoosh-metrics-reporter/lib';

const port = 3002;

MetricsReporterStandalone.listener(port, []);
