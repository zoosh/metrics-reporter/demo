import express, { Express, Request, Response } from 'express';
import { MetricsReporterExpress } from 'zoosh-metrics-reporter/src';

MetricsReporterExpress.reporter('* * * * *', 'Europe/Budapest', 'http://localhost:3000/metrics', []);
