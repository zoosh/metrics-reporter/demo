import express, { Express, Request, Response, NextFunction } from 'express';
import { MetricsReporterExpress } from 'zoosh-metrics-reporter/lib';

const port = 3001;

const app: Express = express();

app.get('/metrics', (req: Request, res: Response, next: NextFunction) => {
  MetricsReporterExpress.listener(req, res, []);
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
});
